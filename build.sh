#!/bin/bash
set -ex
rm -rf build
mkdir build
cd build
cmake ..
cmake --build .
cp tinish bash
cp /bin/bash ./bash.real
# tinish adds TINISH=$argv0 to the environment before exec
./bash ../test.sh -foo |grep TINISH=./bash
