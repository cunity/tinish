#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>

/*
 * exec the real exe by appending ".real" to whatever we have as argv[0]
 * if we are pid == 1, pass this as an argument to tini and exec that
 */

static void print_args(const char** argv) {
  int i;
  for (i = 0; argv[i] != 0x0; i++) {
    fprintf(stderr, "%i '%s'\n", i, argv[i]);
  }
  fprintf(stderr, "\n");
}

int main(int argc, const char** argv) {
  pid_t pid = getpid();
  int rv = 0;
  char real_argv0[PATH_MAX] = {0};
  char tinish_env[PATH_MAX] = {0};
  sprintf(real_argv0, "%s.real", argv[0]);
  sprintf(tinish_env, "TINISH=%s", argv[0]);
  putenv(tinish_env);

  if (pid == 1) {
    const char **new_args = malloc(3 + argc); /* add 2 new words at the start, plus the NULL at the end*/
    if (new_args)
    {
      memset(new_args, 0, sizeof(*new_args));
      int i;
      new_args[0] = "tini";
      new_args[1] = "--";
      new_args[2] = real_argv0;
      new_args[2 + argc] = NULL;  /* execvp wants the last word to be null */
      for (i = 1; i < argc; i++) {
        new_args[2 + i] = argv[i];
      }

      execvp(new_args[0], (char *const*) new_args);
      fprintf(stderr, ".. failed to launch tini.. errno=%d\n", errno);
      print_args(new_args);
      return 1;
    }
  }
  argv[0] = real_argv0;
  execvp(argv[0], (char *const*) argv);
  fprintf(stderr, "execvp of %s failed, errno=%d\n", argv[0], errno);
  print_args(argv);
  return 1; /* never reached */
}
