#!/bin/sh
#
# Replaces ${TINISH_REPLACE} (defaults to /bin/sh) with tinish,
# Warning, this might totally break your system and make it unbootable!
# This is really only intended for use inside docker images.
#
TINISH_REPLACE=${TINISH_REPLACE:-/bin/sh}

set -ex

if [ -f ${TINISH_REPLACE}.real ]
then
  echo ${TINISH_REPLACE}.real already exists!
  exit 1
fi

tinish=$(which tinish)

echo checking that tini is in /bin
ls -l /bin/tini

echo checking that tini works
/bin/tini --version

echo moving ${TINISH_REPLACE}
mv ${TINISH_REPLACE} ${TINISH_REPLACE}.real

echo installing tinish as ${TINISH_REPLACE}
cp ${tinish} ${TINISH_REPLACE}
echo setting new ${TINISH_REPLACE} mode
chmod 755 ${TINISH_REPLACE}

echo checking sh still works

${TINISH_REPLACE} -c 'set -e;ps -ef'

echo all done!
